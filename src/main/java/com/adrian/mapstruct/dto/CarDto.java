package com.adrian.mapstruct.dto;

import lombok.Data;

@Data
public class CarDto {

  private String brand;
  private String model;
  private int numberOfDoors;
  private String color;

}
