package com.adrian.mapstruct.dto;

import lombok.Data;

@Data
public class PetDto {

  private String name;
  private String coatColor;

}
