package com.adrian.mapstruct.dto;

import lombok.Data;

@Data
public class EbookDto {

  /**
   * Datos del libro
   */
  private String ebookName;
  private Long quantityPages;

  /**
   * Datos del Autor
   */
  private String authorName;
  private Long age;

}
