package com.adrian.mapstruct.dto;

import lombok.Data;

@Data
public class UserDto {
  private String username;
  private String password;
  private String role;
}
