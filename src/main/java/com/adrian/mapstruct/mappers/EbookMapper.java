package com.adrian.mapstruct.mappers;

import com.adrian.mapstruct.domain.Ebook;
import com.adrian.mapstruct.dto.EbookDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface EbookMapper {

  @Mapping(source = "ebookName", target = "name")
  @Mapping(source = "authorName", target = "author.name")
  @Mapping(source = "age", target = "author.age")
  Ebook ebookDtoToEbook(EbookDto ebookDto);

}
