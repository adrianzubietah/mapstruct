package com.adrian.mapstruct.mappers;

import com.adrian.mapstruct.domain.Pet;
import com.adrian.mapstruct.dto.PetDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Locale;

@Mapper(componentModel = "spring")
public interface PetMapper {


  @Mapping(source = "name", target = "standardName", qualifiedByName = "normalizeName")
  @Mapping(source = "coatColor", target = "standardCoatColor", qualifiedByName = "normalizeCoatColor")
  Pet petDtoToPet(PetDto petDto);

  @Named("normalizeName")
  static String normalizeName(String name) {
    return name.toUpperCase();
  }

  @Named("normalizeCoatColor")
  static String normalizeCoatColor(String coatColor) {
    return coatColor.toLowerCase();
  }

}
