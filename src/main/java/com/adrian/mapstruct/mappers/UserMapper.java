package com.adrian.mapstruct.mappers;

import com.adrian.mapstruct.domain.User;
import com.adrian.mapstruct.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

  @Mapping(target = "password", source = "password", ignore = true)
  UserDto userToUserDto(User user);

}
