package com.adrian.mapstruct.mappers;

import com.adrian.mapstruct.domain.Car;
import com.adrian.mapstruct.dto.CarDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CarMapper {

  Car carDtoToCar(CarDto carDto);

}
