package com.adrian.mapstruct.domain;

import lombok.Data;

@Data
public class Pet {

  private String name;
  private String standardName;
  private String coatColor;
  private String standardCoatColor;

}
