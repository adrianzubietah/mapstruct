package com.adrian.mapstruct.domain;

import lombok.Data;

@Data
public class Ebook {
  private String name;
  private Long quantityPages;
  private Author author;
}
