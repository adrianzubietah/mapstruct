package com.adrian.mapstruct.domain;

import lombok.Data;

@Data
public class Car {

  private String brand;
  private String model;
  private int numberOfDoors;
  private String color;

}
