package com.adrian.mapstruct.domain;

import lombok.Data;

@Data
public class Author {
  private String name;
  private Long age;
}
