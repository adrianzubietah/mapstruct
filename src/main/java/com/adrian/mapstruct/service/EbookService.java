package com.adrian.mapstruct.service;

import com.adrian.mapstruct.domain.Ebook;
import com.adrian.mapstruct.dto.EbookDto;

public interface EbookService {

  Ebook transformWithInjection(EbookDto ebookDto);

  Ebook transformWithStatic(EbookDto ebookDto);
}
