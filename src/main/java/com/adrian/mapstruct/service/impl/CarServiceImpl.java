package com.adrian.mapstruct.service.impl;

import com.adrian.mapstruct.domain.Car;
import com.adrian.mapstruct.dto.CarDto;
import com.adrian.mapstruct.mappers.CarMapper;
import com.adrian.mapstruct.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

  private final CarMapper carMapper;

  @Override
  public Car carDtoToCar(CarDto carDto) {
    return carMapper.carDtoToCar(carDto);
  }
}
