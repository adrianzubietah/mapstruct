package com.adrian.mapstruct.service.impl;

import com.adrian.mapstruct.domain.User;
import com.adrian.mapstruct.dto.UserDto;
import com.adrian.mapstruct.mappers.UserMapper;
import com.adrian.mapstruct.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserMapper userMapper;

  @Override
  public UserDto userToUserDto(User user) {
    return userMapper.userToUserDto(user);
  }
}
