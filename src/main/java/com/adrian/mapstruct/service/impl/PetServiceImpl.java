package com.adrian.mapstruct.service.impl;

import com.adrian.mapstruct.domain.Pet;
import com.adrian.mapstruct.dto.PetDto;
import com.adrian.mapstruct.mappers.PetMapper;
import com.adrian.mapstruct.service.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PetServiceImpl implements PetService {

  private final PetMapper petMapper;

  @Override
  public Pet petDtoToPet(PetDto petDto) {
    return petMapper.petDtoToPet(petDto);
  }
}
