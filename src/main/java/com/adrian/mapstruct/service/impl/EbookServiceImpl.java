package com.adrian.mapstruct.service.impl;

import com.adrian.mapstruct.domain.Ebook;
import com.adrian.mapstruct.dto.EbookDto;
import com.adrian.mapstruct.mappers.EbookMapper;
import com.adrian.mapstruct.mappers.EbookStaticMapper;
import com.adrian.mapstruct.service.EbookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EbookServiceImpl implements EbookService {

  private final EbookMapper ebookMapper;

  @Override
  public Ebook transformWithInjection(EbookDto ebookDto) {
    return ebookMapper.ebookDtoToEbook(ebookDto);
  }

  @Override
  public Ebook transformWithStatic(EbookDto ebookDto) {
    return EbookStaticMapper.INSTANCE.ebookDtoToEbook(ebookDto);
  }
}
