package com.adrian.mapstruct.service;

import com.adrian.mapstruct.domain.Pet;
import com.adrian.mapstruct.dto.PetDto;

public interface PetService {
  Pet petDtoToPet(PetDto petDto);
}
