package com.adrian.mapstruct.service;

import com.adrian.mapstruct.domain.Car;
import com.adrian.mapstruct.dto.CarDto;

public interface CarService {
  Car carDtoToCar(CarDto carDto);
}
