package com.adrian.mapstruct.service;

import com.adrian.mapstruct.domain.User;
import com.adrian.mapstruct.dto.UserDto;

public interface UserService {
  UserDto userToUserDto(User user);
}
