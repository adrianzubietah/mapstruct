package com.adrian.mapstruct.service;

import com.adrian.mapstruct.MapstructApplicationTests;
import com.adrian.mapstruct.domain.Pet;
import com.adrian.mapstruct.dto.PetDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PetServiceTest extends MapstructApplicationTests {

  @Autowired
  private PetService petService;

  @Test
  public void petDtoToPet_withAllFields() {
    PetDto petDto = new PetDto();
    petDto.setName("neron");
    petDto.setCoatColor("BlaNco");

    Pet pet = petService.petDtoToPet(petDto);

    assertEquals(petDto.getName(), pet.getName());
    assertEquals(petDto.getName().toUpperCase(), pet.getStandardName());
    assertEquals(petDto.getCoatColor(), pet.getCoatColor());
    assertEquals(petDto.getCoatColor().toLowerCase(), pet.getStandardCoatColor());
  }
}
