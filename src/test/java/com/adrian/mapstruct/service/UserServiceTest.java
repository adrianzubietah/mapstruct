package com.adrian.mapstruct.service;

import com.adrian.mapstruct.MapstructApplicationTests;
import com.adrian.mapstruct.domain.Car;
import com.adrian.mapstruct.domain.User;
import com.adrian.mapstruct.dto.CarDto;
import com.adrian.mapstruct.dto.UserDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class UserServiceTest extends MapstructApplicationTests {

  @Autowired
  private UserService userService;

  @Test
  public void userToUserDto_withPassword_returnUserDtoWithoutPassword() {
    User user = new User();
    user.setUsername("admin");
    user.setPassword("123456");
    user.setRole("Adminitrador");

    UserDto userDto = userService.userToUserDto(user);

    assertEquals(userDto.getUsername(), user.getUsername());
    assertEquals(userDto.getRole(), user.getRole());
    assertNull(userDto.getPassword());

  }
}
