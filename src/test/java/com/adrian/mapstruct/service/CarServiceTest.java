package com.adrian.mapstruct.service;

import com.adrian.mapstruct.MapstructApplicationTests;
import com.adrian.mapstruct.domain.Car;
import com.adrian.mapstruct.dto.CarDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CarServiceTest extends MapstructApplicationTests {

  @Autowired
  private CarService carService;

  @Test
  public void carDtoToCar_withAllFields() {
    CarDto carDto = new CarDto();
    carDto.setBrand("Peugeot");
    carDto.setModel("3008");
    carDto.setColor("Black");
    carDto.setNumberOfDoors(4);

    Car car = carService.carDtoToCar(carDto);

    assertEquals(car.getBrand(), carDto.getBrand());
    assertEquals(car.getModel(), carDto.getModel());
    assertEquals(car.getColor(), carDto.getColor());
    assertEquals(car.getNumberOfDoors(), carDto.getNumberOfDoors());

  }
}
