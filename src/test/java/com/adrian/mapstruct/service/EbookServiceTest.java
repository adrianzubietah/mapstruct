package com.adrian.mapstruct.service;

import com.adrian.mapstruct.MapstructApplicationTests;
import com.adrian.mapstruct.domain.Ebook;
import com.adrian.mapstruct.dto.EbookDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EbookServiceTest extends MapstructApplicationTests {

  @Autowired
  public EbookService ebookService;

  @Test
  public void ebookDtoToEbook_withStatic() {
    EbookDto ebookDto = new EbookDto();
    ebookDto.setEbookName("Harry Potter");
    ebookDto.setQuantityPages(1400L);
    ebookDto.setAuthorName("Rowling, J.K");
    ebookDto.setAge(55L);

    Ebook ebook = ebookService.transformWithStatic(ebookDto);

    assertEquals(ebookDto.getEbookName(), ebook.getName());
    assertEquals(ebookDto.getQuantityPages(), ebook.getQuantityPages());
    assertEquals(ebookDto.getAuthorName(), ebook.getAuthor().getName());
    assertEquals(ebookDto.getAge(), ebook.getAuthor().getAge());
  }

  @Test
  public void ebookDtoToEbook_withoutStatic() {

    EbookDto ebookDto = new EbookDto();
    ebookDto.setEbookName("Harry Potter");
    ebookDto.setQuantityPages(1400L);
    ebookDto.setAuthorName("Rowling, J.K");
    ebookDto.setAge(55L);

    Ebook ebook = ebookService.transformWithInjection(ebookDto);

    assertEquals(ebookDto.getEbookName(), ebook.getName());
    assertEquals(ebookDto.getQuantityPages(), ebook.getQuantityPages());
    assertEquals(ebookDto.getAuthorName(), ebook.getAuthor().getName());
    assertEquals(ebookDto.getAge(), ebook.getAuthor().getAge());
  }

}
